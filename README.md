## Purpose

Sviluppo del sistema os161 per il corso Programmazione di Sistema 2016/2017, Politecnico di Torino.

## About  

**os161** è un sistema operativo open source che, in forma semplificata, emula i componenti di un moderno sistema operativo.
Il sistema è virtualizzato all'interno della macchina virtuale **sys161**, che emula l'hardware di un calcolatore con microprocessore MIPS r3000.

Maggiori informazioni ed elenco degli assignment originali: https://www.ops-class.org/ 
