#include <types.h>
#include <kern/unistd.h>
#include <kern/errno.h>
#include <kern/wait.h>
#include <lib.h>
#include <syscall.h>
#include <thread.h>
#include <proc.h>
#include <current.h>            // for curthread and/or curproc
#include <synch.h>

int sys_exit(int status) {

    struct proc *cur = curproc;

    KASSERT(proc_table[cur->pid] != NULL);

    lock_acquire(cur->exit_lk);

    // TO DO: chiudo tutti i file descriptor
    
    if(proc_table[cur->ppid]->exit_flag == false) {
        
        cur->exit_flag = true;
        cur->exit_status = _MKWAIT_EXIT(status);

        cv_signal(cur->exit_cv, cur->exit_lk);
        lock_release(cur->exit_lk);
    } 
    /*
     * Caso in cui il padre invece sia già terminato: a noi non accade
     * ma si dovrebbe eliminare già qui la struttura.
     */
    else {}

    thread_exit();
    return 0;
}

int sys_waitpid(pid_t pid, int *status, int options, int *retval) {
    
    struct proc *proc = proc_table[pid];
    *retval = -1;

    if(options != 0 && options != WNOHANG) {
        return EINVAL;
    }

    if(pid < PID_MIN || pid > PID_MAX) {
        *retval = ESRCH;
        return -1;
    }

    if(proc_table[pid] == NULL){
        *retval = ESRCH;
        return -1;
    }

    if(curproc->pid != proc_table[pid]->ppid ){
        *retval = ECHILD;
        return -1;
    }

    lock_acquire(proc->exit_lk);

    // Il processo di cui è stata chiamata la wait è già finito
    if(proc->exit_status != -1) {

        (*status) = proc->exit_status;
        lock_release(proc->exit_lk);

    } 
    // Il processo non è ancora terminato, discrimino se aspettare o meno
    else {
        if(options == WNOHANG) {
            lock_release(proc->exit_lk);
            return 0;
        }
        cv_wait(proc->exit_cv, proc->exit_lk);
    }

    lock_release(proc->exit_lk);

    (*status) = proc->exit_status;
    *retval = 0;

    proc_destroy(proc);
    return 0;
}
