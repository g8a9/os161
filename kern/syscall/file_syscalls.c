#include <types.h>
#include <kern/unistd.h>
#include <kern/errno.h>
#include <kern/fcntl.h>
#include <lib.h>
#include <syscall.h>
#include <thread.h>
#include <proc.h>
#include <current.h>            // for curthread and/or curproc
#include <vfs.h>
#include <uio.h>
#include <vnode.h>
#include <copyinout.h>

int sys_write(int fd, void *buf, size_t size, int *retval) {
    char *tmp;
    int result;

    if(fd < 0 || fd >= OPEN_MAX) {
        *retval = -1;
        return EBADF;
    }
    
    if(fd == STDOUT_FILENO) {        
        
        tmp = kstrdup((char *) buf);
        tmp[size] = '\0';

        kprintf("%s", tmp);
        
        *retval = strlen(tmp);

        kfree(tmp);
        return 0;    
    } 

    // Non è lo stdout: si tratta di un file aperto.
    if(curproc->file_table[fd] == NULL) {
        *retval = -1;
        return EBADF;
    }

    if(curproc->file_table[fd]->fh_flags & O_RDONLY) {
        *retval = -1;
        return EROFS;
    }

    struct iovec iov;
    struct uio u;

    iov.iov_ubase = buf;
    iov.iov_len = size;

    u.uio_iov = &iov;
    u.uio_iovcnt = 1;
    u.uio_offset = curproc->file_table[fd]->fh_offset;
    u.uio_resid = size;
    u.uio_segflg = UIO_USERSPACE;
    u.uio_rw = UIO_WRITE;
    u.uio_space = curproc->p_addrspace;

    result = VOP_WRITE(curproc->file_table[fd]->fh_vnode, &u);
    if(result) {
        *retval = -1;
        return EBADF;
    }

    // Il puntatore all'interno del file viene spostato avanti nella struttura
    curproc->file_table[fd]->fh_offset = u.uio_offset;

    // uio_resid viene decrementato dei byte letti
    *retval = (int) size - u.uio_resid;
    return 0;
}

int sys_read(int fd, void *buf, size_t size, int *retval) {
    int result;
    
    if(fd < 0 || fd >= OPEN_MAX) {
        *retval = -1;
        return EBADF;
    }

    if(fd == STDIN_FILENO) {
        kgets((char *) buf, size);
        
        *retval = strlen(buf); 
        
        return 0;
    } 

    // Non è lo stdin: si tratta di un file aperto.
    if(curproc->file_table[fd] == NULL) {
        *retval = -1;
        return EBADF;
    }

    if(curproc->file_table[fd]->fh_flags & O_WRONLY) {
        *retval = -1;
        return EROFS;
    }

    struct iovec iov;
    struct uio u;

    iov.iov_ubase = buf;
    iov.iov_len = size;

    u.uio_iov = &iov;
    u.uio_iovcnt = 1;
    u.uio_offset = curproc->file_table[fd]->fh_offset;
    u.uio_resid = size;
    u.uio_segflg = UIO_USERSPACE;
    u.uio_rw = UIO_READ;
    u.uio_space = curproc->p_addrspace;

    result = VOP_READ(curproc->file_table[fd]->fh_vnode, &u);
    if(result) {
        *retval = -1;
        return EBADF;
    }

    // Il puntatore all'interno del file viene spostato avanti nella struttura
    curproc->file_table[fd]->fh_offset = u.uio_offset;

    // uio_resid viene decrementato dei byte letti
    *retval = (int) size - u.uio_resid;
    return 0;
}

int sys_open(const char *filename, int flags, mode_t mode, int *retval) {

    char *kfilename;
    int result, fd;

    fd = 3;
    while(curproc->file_table[fd] != NULL && fd < OPEN_MAX)
        fd++;
    
    if(fd == OPEN_MAX) {
        *retval = -1;
        return EMFILE;
    }

    kfilename = kmalloc(sizeof(char) * NAME_MAX);
    result = copyinstr((const_userptr_t) filename, kfilename, NAME_MAX, NULL);
    if(result) {
        kfree(kfilename);
        *retval = -1;
        return result;
    }

    curproc->file_table[fd] = kmalloc(sizeof(struct file_handler));
    curproc->file_table[fd]->fh_flags = flags;
    curproc->file_table[fd]->fh_offset = 0;
    
    result = vfs_open(kfilename, flags, mode, &curproc->file_table[fd]->fh_vnode);
    if(result) {
        kfree(kfilename);
        kfree(curproc->file_table[fd]);
        *retval = -1;
        return result;
    }

    kfree(kfilename);
    *retval = fd;
    return 0;
}

int sys_close(int fd, int *retval) {

    if(fd < 0 || fd >= OPEN_MAX) {
        *retval = -1;
        return EBADF;
    }

    if(curproc->file_table[fd] == NULL) {
        *retval = -1;
        return EBADF;
    }

    vfs_close(curproc->file_table[fd]->fh_vnode);
    kfree(curproc->file_table[fd]);
    curproc->file_table[fd] = NULL;

    *retval = 0;
    return 0;
}
